from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index, retrieve

# Create your tests here.

# URL Testcases
class UrlsTest(TestCase):
    def test_url_exist_home(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_api_book(self):
        response = Client().get('/api/books/andhika/')
        self.assertEqual(response.status_code, 200)

# View Testcases 
class ViewsTest(TestCase):
    def test_view_home_is_using_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_view_retreive_is_using_function_retrieve(self):
        found = resolve('/api/books/andhika/')
        self.assertEqual(found.func, retrieve)

# Template Testcases
class TemplatesTest(TestCase):
    def test_template_home_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "home/index.html")




