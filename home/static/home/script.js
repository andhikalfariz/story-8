// console.log("JS masuk");
$("#keyword").keyup( function() {
    // console.log("works!");
    var isi_keyword = $("#keyword").val();
    // console.log(isi_keyword);
    var url_untuk_dipanggil = "api/books/" + isi_keyword;
    // console.log(url_untuk_dipanggil);
    $.ajax({
        url: url_untuk_dipanggil,
        success: function(hasil) {
            // console.log(hasil.items);
            var object_hasil = $("#hasil");
            object_hasil.empty();

            for (i=0; i<hasil.items.length; i++) {
                var tmp_title = hasil.items[i].volumeInfo.title;
                var tmp_thumbnails = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                var tmp_link = hasil.items[i].volumeInfo.previewLink;
                // console.log(tmp_link);
                object_hasil.append(
                    '<tr> <td class="cover"> <img src =' + tmp_thumbnails + '></td> <td class="title">' + tmp_title + '</td><td class="link"> <a href=' + tmp_link + '> Baca Selengkapnya</a></td></tr>'
                    )
            }   
        }
    });
});
